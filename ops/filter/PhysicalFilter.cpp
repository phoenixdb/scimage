#include "log4cxx/logger.h"

#include "query/Operator.h"
#include "array/Metadata.h"
#include "array/MemArray.h"
#include "array/FileArray.h"
#include "query/Network.h"
#include "util/Barrier.h"
#include <util/JobQueue.h>
#include <util/ThreadPool.h>

using namespace scidb;

namespace scimage
{

// Logger for operator. static to prevent visibility of variable outside of file
static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("filter.scimage"));

class PhysicalFilter: public PhysicalOperator
{
private:
    struct ScimageChunk
    {
        int size_rows;
        int size_colls;
        unsigned char color_depth;
        unsigned char *data;
    };

public:
    PhysicalFilter(const std::string& logicalName, const std::string& physicalName, const Parameters& parameters, const ArrayDesc& schema):
        PhysicalOperator(logicalName, physicalName, parameters, schema)
    {
    }

    void filter(ScimageChunk *ch, ScimageChunk *out_ch)
    {
        out_ch->color_depth = (unsigned char)1;
        out_ch->size_rows = ch->size_rows;
        out_ch->size_colls = ch->size_colls;

        for(int i = 0; i < ch->size_rows; i++) {
            for(int j = 0; j < ch->size_colls; j++) {
                float bright = 0.0;
                int start_i = i, end_i = i, start_j = j, end_j = j, count = 0;
                if(i != 0){
                    start_i = i - 1;
                }
                if(i != ch->size_rows - 1){
                    end_i = i + 1;
                }
                if(j != 0){
                    start_j = j - 1;
                }
                if(j != ch->size_colls - 1){
                    end_j = j + 1;
                }
                for(int k = start_i; k <= end_i; k++){
                    for(int n = start_j; n <= end_j; n++){
                        int index = k * ch->size_colls + n;
                        bright += ch->data[index];
                        count++;
                        if(k == i && n == j){
                            bright += ch->data[index];
                            count++;
                        }
                    }
                }
                int index = i * ch->size_colls + j;
                out_ch->data[index] = (unsigned char)((int)(bright / count));
            }
        }
    }

    shared_ptr<Array> execute(std::vector<shared_ptr<Array> >& inputArrays, shared_ptr<Query> query)
    {
        shared_ptr<Array> array = inputArrays[0];
        const Dimensions& dims = array->getArrayDesc().getDimensions();

        shared_ptr<Array> resultArray = createTmpArray(_schema);
        boost::shared_ptr<ConstArrayIterator> inputIter;
        boost::shared_ptr<ArrayIterator> outputIter;

        inputIter = array->getConstIterator(0);
        outputIter = resultArray->getIterator(0);

        while (!inputIter->end())
        {
            Coordinates begin = inputIter->getPosition();
            Coordinates end(2);
            end[0] = begin[0] + dims[0].getChunkInterval() - 1;
            end[1] = begin[1] + dims[1].getChunkInterval() - 1;
            const size_t blockSize = dims[0].getChunkInterval() * dims[1].getChunkInterval();
            vector<char> block(blockSize);
            array->extractData(0, (void*)&block[0], begin, end);

            Chunk& chunk = outputIter->newChunk(begin);
            chunk.setRLE(false);
            chunk.allocate(blockSize * sizeof(char));

            ScimageChunk ic;
            ic.color_depth = 1;
            ic.size_colls = dims[0].getChunkInterval();
            ic.size_rows = dims[1].getChunkInterval();
            ic.data = (unsigned char*)&block[0];

            ScimageChunk rc = ic;
            rc.data = (unsigned char*)chunk.getData();

            filter(&ic, &rc);

            chunk.write(query);

            ++(*inputIter);
        }

        LOG4CXX_DEBUG(logger, "Filter operator is over")

        return resultArray;
    }
};

REGISTER_PHYSICAL_OPERATOR_FACTORY(PhysicalFilter, "scimage_filter", "scimage_filter_impl");

} //namespace
