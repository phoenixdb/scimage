#include "log4cxx/logger.h"

#include "query/Operator.h"
#include "array/Metadata.h"
#include "array/MemArray.h"
#include "array/FileArray.h"
#include "query/Network.h"
#include "util/Barrier.h"
#include <util/JobQueue.h>
#include <util/ThreadPool.h>

using namespace scidb;

namespace scimage
{

// Logger for operator. static to prevent visibility of variable outside of file
static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("contour.scimage"));

class PhysicalContour: public PhysicalOperator
{
private:

    struct ScimageChunk{
        int size_rows;
        int size_colls;
        unsigned char color_depth;
        unsigned char *data;
    };

public:

    PhysicalContour(const std::string& logicalName, const std::string& physicalName, const Parameters& parameters, const ArrayDesc& schema):
        PhysicalOperator(logicalName, physicalName, parameters, schema) {
    }

    void contour(ScimageChunk *ch, ScimageChunk *out_ch, int diff) {
        out_ch->color_depth = (unsigned char)1;
        out_ch->size_rows = ch->size_rows;
        out_ch->size_colls = ch->size_colls;
        for(int i = 0; i < ch->size_rows; i++) {
            for(int j = 0; j < ch->size_colls; j++) {
                int index = i * ch->size_colls + j;
                if((ch->data[index] - ch->data[index + 1] > diff) && (j != ch->size_colls - 1)) {
                    out_ch->data[index] = (unsigned char)0;
                } else if((ch->data[index] - ch->data[index - 1] > diff) && (j != 0)) {
                    out_ch->data[index] = (unsigned char)0;
                } else if((ch->data[index] - ch->data[index + ch->size_colls] > diff) && (i != ch->size_rows - 1)) {
                    out_ch->data[index] = (unsigned char)0;
                } else if((ch->data[index] - ch->data[index - ch->size_colls] > diff) && (i != 0)) {
                    out_ch->data[index] = (unsigned char)0;
                } else {
                    out_ch->data[index] = (unsigned char)255;
                }
            }
        }
    }

    shared_ptr<Array> execute(std::vector<shared_ptr<Array> >& inputArrays, shared_ptr<Query> query) {
        shared_ptr<Array> array = inputArrays[0];
        const Dimensions& dims = array->getArrayDesc().getDimensions();

        shared_ptr<Array> resultArray = createTmpArray(_schema);
        boost::shared_ptr<ConstArrayIterator> inputIter;
        boost::shared_ptr<ArrayIterator> outputIter;

        inputIter = array->getConstIterator(0);
        outputIter = resultArray->getIterator(0);

        while (!inputIter->end()) {
            Coordinates begin = inputIter->getPosition();
            Coordinates end(2);
            end[0] = begin[0] + dims[0].getChunkInterval() - 1;
            end[1] = begin[1] + dims[1].getChunkInterval() - 1;
            const size_t blockSize = dims[0].getChunkInterval() * dims[1].getChunkInterval();
            vector<char> block(blockSize);
            array->extractData(0, (void*)&block[0], begin, end);

            Chunk& chunk = outputIter->newChunk(begin);
            chunk.setRLE(false);
            chunk.allocate(blockSize * sizeof(char));

            ScimageChunk ic;
            ic.color_depth = 1;
            ic.size_colls = dims[0].getChunkInterval();
            ic.size_rows = dims[1].getChunkInterval();
            ic.data = (unsigned char*)&block[0];

            ScimageChunk rc = ic;
            rc.data = (unsigned char*)chunk.getData();

            int diff;
            diff = ((boost::shared_ptr<OperatorParamPhysicalExpression>&)_parameters[0])->getExpression()->evaluate().getUint8();

            contour(&ic, &rc, diff);

            chunk.write(query);

            ++(*inputIter);
        }

        LOG4CXX_DEBUG(logger, "Contour operator is over")

        return resultArray;
    }
};

REGISTER_PHYSICAL_OPERATOR_FACTORY(PhysicalContour, "scimage_contour", "scimage_contour_impl");

} //namespace
