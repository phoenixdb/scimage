#include "log4cxx/logger.h"

#include "query/Operator.h"
#include "array/Metadata.h"
#include "array/MemArray.h"
#include "array/FileArray.h"
#include "query/Network.h"
#include "util/Barrier.h"
#include <util/JobQueue.h>
#include <util/ThreadPool.h>

using namespace scidb;

namespace scimage
{

// Logger for operator. static to prevent visibility of variable outside of file
static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("diff_filter.scimage"));

class PhysicalDiffFilter: public PhysicalOperator
{
private:
    struct ScimageChunk
    {
        int size_rows;
        int size_colls;
        unsigned char color_depth;
        unsigned char *data;
    };

public:
    PhysicalDiffFilter(const std::string& logicalName, const std::string& physicalName, const Parameters& parameters, const ArrayDesc& schema):
        PhysicalOperator(logicalName, physicalName, parameters, schema)
    {
    }

    void diff_filter(ScimageChunk *ch, ScimageChunk *out_ch)
    {
        out_ch->color_depth = (unsigned char)1;
        out_ch->size_rows = ch->size_rows;
        out_ch->size_colls = ch->size_colls;

        for(int i = 0; i < ch->size_rows; i++) {
            for(int j = 0; j < ch->size_colls; j++) {
                float bright = 0.0;
                int index = i * ch->size_colls + j, count = 1;
                if(i != 0){
                    bright -= ch->data[(i - 1) * ch->size_colls + j];
                    count++;
                }
                if(i != ch->size_rows - 1){
                    bright -= ch->data[(i + 1) * ch->size_colls + j];
                    count++;
                }
                if(j != 0){
                    bright -= ch->data[i * ch->size_colls + j - 1];
                    count++;
                }
                if(j != ch->size_colls - 1){
                    bright -= ch->data[i * ch->size_colls + j + 1];
                    count++;
                }
                bright += ch->data[index] * count;
                if(bright < 0) {
                    out_ch->data[index] = (unsigned char)0;
                }
                else if(bright > 255) {
                    out_ch->data[index] = (unsigned char)255;
                }
                else {
                    out_ch->data[index] = (unsigned char)((int)bright);
                }
            }
        }
    }

    shared_ptr<Array> execute(std::vector<shared_ptr<Array> >& inputArrays, shared_ptr<Query> query)
    {
        shared_ptr<Array> array = inputArrays[0];
        const Dimensions& dims = array->getArrayDesc().getDimensions();

        shared_ptr<Array> resultArray = createTmpArray(_schema);
        boost::shared_ptr<ConstArrayIterator> inputIter;
        boost::shared_ptr<ArrayIterator> outputIter;

        inputIter = array->getConstIterator(0);
        outputIter = resultArray->getIterator(0);

        while (!inputIter->end())
        {
            Coordinates begin = inputIter->getPosition();
            Coordinates end(2);
            end[0] = begin[0] + dims[0].getChunkInterval() - 1;
            end[1] = begin[1] + dims[1].getChunkInterval() - 1;
            const size_t blockSize = dims[0].getChunkInterval() * dims[1].getChunkInterval();
            vector<char> block(blockSize);
            array->extractData(0, (void*)&block[0], begin, end);

            Chunk& chunk = outputIter->newChunk(begin);
            chunk.setRLE(false);
            chunk.allocate(blockSize * sizeof(char));

            ScimageChunk ic;
            ic.color_depth = 1;
            ic.size_colls = dims[0].getChunkInterval();
            ic.size_rows = dims[1].getChunkInterval();
            ic.data = (unsigned char*)&block[0];

            ScimageChunk rc = ic;
            rc.data = (unsigned char*)chunk.getData();

            diff_filter(&ic, &rc);

            chunk.write(query);

            ++(*inputIter);
        }

        LOG4CXX_DEBUG(logger, "DiffFilter operator is over")

        return resultArray;
    }
};

REGISTER_PHYSICAL_OPERATOR_FACTORY(PhysicalDiffFilter, "scimage_diff_filter", "scimage_diff_filter_impl");

} //namespace
