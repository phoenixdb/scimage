#include "query/Operator.h"

using namespace scidb;

namespace scimage
{

class LogicalDiffFilter: public LogicalOperator
{
public:
    LogicalDiffFilter(const std::string& logicalName, const std::string& alias):
        LogicalOperator(logicalName, alias)
    {
        ADD_PARAM_INPUT()
    }

    ArrayDesc inferSchema(std::vector<ArrayDesc> schemas, boost::shared_ptr<Query> query)
    {
        assert(schemas.size() == 1);

        Attributes atts = schemas[0].getAttributes();

        ArrayDesc schema = schemas[0];
        schema.setName("scimage_diff_filter");

        return schema;
	}

};

REGISTER_LOGICAL_OPERATOR_FACTORY(LogicalDiffFilter, "scimage_diff_filter");

} //namespace
