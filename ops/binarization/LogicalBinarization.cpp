#include "query/Operator.h"

using namespace scidb;

namespace scimage
{

class LogicalBinarization: public LogicalOperator
{
public:
    LogicalBinarization(const std::string& logicalName, const std::string& alias):
        LogicalOperator(logicalName, alias)
    {
        ADD_PARAM_INPUT();
        ADD_PARAM_CONSTANT("uint8");
    }

    ArrayDesc inferSchema(std::vector<ArrayDesc> schemas, boost::shared_ptr<Query> query)
    {
        assert(schemas.size() == 1);

        Attributes atts = schemas[0].getAttributes();

        ArrayDesc schema = schemas[0];
        schema.setName("scimage_binarization");

        return schema;
    }

};

REGISTER_LOGICAL_OPERATOR_FACTORY(LogicalBinarization, "scimage_binarization");

} //namespace
