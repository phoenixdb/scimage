#include "log4cxx/logger.h"

#include "query/Operator.h"
#include "array/Metadata.h"
#include "array/MemArray.h"
#include "array/FileArray.h"
#include "query/Network.h"
#include "util/Barrier.h"
#include <util/JobQueue.h>
#include <util/ThreadPool.h>

using namespace scidb;

namespace scimage
{

// Logger for operator. static to prevent visibility of variable outside of file
static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("stub.scimage"));

class PhysicalStub: public PhysicalOperator
{
public:
    PhysicalStub(const std::string& logicalName, const std::string& physicalName, const Parameters& parameters, const ArrayDesc& schema):
        PhysicalOperator(logicalName, physicalName, parameters, schema)
    {
    }

    shared_ptr<Array> execute(std::vector<shared_ptr<Array> >& inputArrays, shared_ptr<Query> query)
    {
        shared_ptr<Array> array = inputArrays[0];
        const Dimensions& dims = array->getArrayDesc().getDimensions();

        shared_ptr<Array> resultArray = createTmpArray(_schema);
        boost::shared_ptr<ConstArrayIterator> inputIter;
        boost::shared_ptr<ArrayIterator> outputIter;

        inputIter = array->getConstIterator(0);
        outputIter = resultArray->getIterator(0);

        while (!inputIter->end())
        {
            Coordinates begin = inputIter->getPosition();
            Coordinates end(2);
            end[0] = begin[0] + dims[0].getChunkInterval() - 1;
            end[1] = begin[1] + dims[1].getChunkInterval() - 1;
            const size_t blockSize = dims[0].getChunkInterval() * dims[1].getChunkInterval();
            vector<char> block(blockSize);
            array->extractData(0, (void*)&block[0], begin, end);

            Chunk& chunk = outputIter->newChunk(begin);
            chunk.setRLE(false);
            chunk.allocate(blockSize * sizeof(char));
            char *result = (char*)chunk.getData();
            memcpy(result, &block[0], chunk.getSize());
            chunk.write(query);

            ++(*inputIter);
        }

        LOG4CXX_DEBUG(logger, "Stub operator is over")

        return resultArray;
    }
};

REGISTER_PHYSICAL_OPERATOR_FACTORY(PhysicalStub, "scimage_stub", "scimage_stub_impl");

} //namespace
