// 111.cpp: îïðåäåëÿåò òî÷êó âõîäà äëÿ êîíñîëüíîãî ïðèëîæåíèÿ.
//

//#include "stdafx.h"
#include "iostream"
//#include "cv.h"
//#include "highgui.h"


using namespace std;

//ðàçìåðíîñòü âõîäÿùåãî ìàññèâà
////const int height = 5;
//const int width = 7;

const int height = 10;
const int width = 13;

// ðàçìåðû êóñêà
int size_Chunk_hor = 7;
int size_Chunk_ver = 7;

// ðàçìåðû ìàòðèöû ìåäèàííîé ôèëüòðàöèè
int  med_Filtr_w = 3;
int  med_Filtr_h = 3;

//Ïîðîã Áèíàðèçàöèè
const int Step = 128;

//âõîäíîé ìàññèâ èç scidb!!!
int input_mass [height][width]; 
//âûõîäíîé ìàññèâ èç scidb
int output_mass [height][width];
//int input_mass_G [height][width]; 
//int input_mass_B [height][width]; 

//ñòðóêòóðà Êóñîê
struct chunk {
	int size_rows;
	int size_colls;
	unsigned char color_depth;
	unsigned char *data_input_buf;
	unsigned char *data_output_buf;
};

//ââîä ìàòðèöû äëÿ òåñòèðîâàíèÿ
void auto_input () {
	int k = 0;
	for (int i = 0; i < height; i ++) {
		for (int j = 0; j< width; j++) {
			input_mass[i][j] = (k++);
			//input_mass[i][j] = (k++);
			//input_mass[i][j] = (k++);
			if (k % 3 == 0)
				input_mass[i][j] += input_mass[i][j];
			if (k == 255)
				k = 0;
			cout << input_mass[i][j] << '\t';
		}
		cout << '\n';
	}

};

void qs(unsigned char *a, int l, int r)
{
	int i = l, j = r; size_t x = a[(r + l) / 2];
	do
	{
		while(a[i] < x)
			i++;
		while(a[j]>x)
			j--;
		if(i <= j)
			swap(a[i++], a[j--]);
	}while(i<=j);

	if (i < r)
		qs(a, i, r);
	if (l < j)
		qs(a, l, j);
};

//ïðîöåäóðà ìåä. ôèëüòðàöèè êóñêîâ
void Med_Filter (chunk *ch, chunk *out_ch) {

	//ïðîâåðêà íà êîððåêòíîñòü (ðàçìåð êóñêà äîëæåí áûòü áîëüøå ðàçìåðà ìàñêè)
	if (ch->size_colls * ch->size_rows < med_Filtr_w * med_Filtr_h)
		return;

	//Îáúÿâëåíèå ìàññèâà ìàñêè
	unsigned char* mass = new unsigned char[med_Filtr_w * med_Filtr_h];
	//îáúÿâëåíèå ìàññèâà, êóäà áóäóò çàíîñèòüñÿ íîâûå çíà÷åíèÿ îòôèëüòðîâàííîãî êóñêà
	unsigned char* new_chunk = new unsigned char[ch->size_rows * ch->size_colls];
	int index = med_Filtr_w * med_Filtr_h / 2 + 1;

	//Öèêë ïî âñåé äëèíå áóôåðà ñ êóñêîì; èíèöèàëèçèðóåì ìàññèâ ñ îòôèëüòðîâàííûì êóñêîì ñòàðûìè çíà÷åíèÿìè
	for (int i = 0; i < ch->size_rows * ch->size_colls; i++){
		new_chunk[i] = ch->data_input_buf[i];
	}

	double y = 0;
	int value = 0;
	//öèêë ïîýëåìåíòíîãî ñìåùåíèÿ âïðàâî ñ íà÷àëà êóñêà
	for (int i = 0; i < ch->size_rows * ch->size_colls - med_Filtr_w * med_Filtr_h + 1; i++){

		int z = 0;
		
		//cout << '\n';
		
		//ôîðìèðîâàíèå íîâîé ìàñêè
		for (int j = i; j < med_Filtr_w * med_Filtr_h + i; j++) {
			mass[z] = ch->data_input_buf[j];
			//âûâîä ýëåìåíòîâ èñõîäíîãî êóñêà
			//cout << (int)mass[z] << '\t';
			z++;
		}
		
		//Âûçîâ ôóíêöèè ñîðòèðîâêè ýëåìåíòîâ ìàñêè
		qs(mass, 0, med_Filtr_w * med_Filtr_h);

		//Çàìåíà öåíòðàëüíîãî ýëåìåíòà â çàõâà÷åííîì ìàñêîé ÷àñòè êóñêà è çàìåíà åãî íà öåíòðàëüíûé ýëåìåíò îòñîðòèðîâàííîé ìàñêè
		new_chunk[index - 1] = mass[med_Filtr_w * med_Filtr_h / 2];
		index++;


		//âûâîä ýëåìåíòîâ îòñîðòèðîâàííîé ìàñêè-----------------------------------------
		//cout << '\n';
		/*for (int j = 0; j < med_Filtr_w * med_Filtr_h; j++) {
			cout << (int)mass[j] << '\t';
		}*/

		//âûâîä ýëåìåíòîâ íîâîãî êóñêà (ñ ìåäèàííîé ôèëüòðàöèåé)
		/*cout << '\n';
		for (int j = i; j < med_Filtr_w * med_Filtr_h + i; j++) {
			cout << (int)new_chunk[j] << '\t';
		}*/

		/*for (int j = 0; j < med_Filtr_w * med_Filtr_h - 1; j++) {
			cout << (int)mass[j] << '\t';
		}*/
		//--------------------------------------------------------------------------------

		
		}

		//Ôîðìèðîâàíèå âûõîäíîãî êóñêà
		out_ch->size_rows = ch->size_rows;
		out_ch->size_colls = ch->size_colls;
		out_ch->color_depth = 3;

		for (int i = 0; i < ch->size_rows * ch->size_colls; i++){
			out_ch->data_output_buf[i] = new_chunk[i];
		}
		

};


int main(int argc, char* argv[])
{
	auto_input ();

	//ïîäñ÷žò êîëè÷åñòâà êóñêîâ--------------------------------------------------------------
	int x = 0;
	int y = 0;

	x = width / size_Chunk_hor;
	if (width % size_Chunk_hor != 0)
		x++;

	y = height / size_Chunk_ver;
	if (height % size_Chunk_ver != 0)
		y++;

	//---------------------------------------------------------------------------------------
	//Êîëè÷åñòâî êóñêîâ, íà êîòîðîå áóäåò ðàçäåëží ìàññèâ
	int count_Chunk = x * y;

	//Èñõîäíûé ìàññèâ êóñêîâ
	chunk *array_chanks = new chunk [count_Chunk];

	chunk *array_out_chanks = new chunk [count_Chunk];

	//Âõîäíîé è âûõîäíîé áóôåð, ïåðåäàâåìûé â ôóíêöèþ Binarization---------------------------
	unsigned char * buffer = new unsigned char [size_Chunk_hor*size_Chunk_ver];
	unsigned char * buffer_out = new unsigned char [size_Chunk_hor*size_Chunk_ver];

	//Îñóùåñòâëÿåì èíèöèàëèçàöèþ àòðèáóòîâ-äàííûõ ìàññèâà îáúåêòîâ ñòðóêòóðû chunk-----------
	int a = 0;
	int b = 0;
	cout << '\n';
	for (int i = 0; i < count_Chunk; i++) {
		array_chanks[i].size_rows = size_Chunk_ver; // âûñîòà êóñêà (êîëè÷åñòâî ñòðîê) 
		array_chanks[i].size_colls = size_Chunk_hor;//äëèíà ñòðîêè êóñêà	
		array_chanks[i].color_depth = 3;
		array_chanks[i].data_input_buf = buffer;
		array_out_chanks[i].data_output_buf = buffer_out;

		//array_out_chanks[i].
	}

	//Ïðîöåññ çàïîëíåíèÿ áóôåðà è ïåðåäà÷è åãî íà îáðàáîòêó-------------------------------------
	cout << '\n';
	int m = 0;
	int n = 0;
	int m1 = 0;
	int n1 = 0;
	int l = 0;
	int l1 = 0;
	//int counter = 0;
	int size_ver = 0;
	int size_hor = 0;
	//Öèêë ïî êóñêàì
	for (int i = 0; i < count_Chunk; i++) {

		//Öèêë ïî ñòîðîêàì êóñêà
		for (int j = 0; j < size_Chunk_ver; j++) {

			if (n * size_Chunk_ver + j >= height)
				continue;

			//Öèêë ïî ñòîëáöàì êóñêà
			for (int k = 0; k < size_Chunk_hor; k++) {
				
				//Îáðàáîòêà ñèòóàöèè, êîãäà ðàçìåð êóñêà âûõîäèò çà ãðàíèöû èçîáðàæåíèÿ
				if ((j + n * size_Chunk_ver > (height - 1)) || (k + m * size_Chunk_hor > (width - 1)))
				{
						continue;
				}
				
				//Çàïîëíÿåì ïåðâûé (âõîäíîé) áóôåð ýëåìåíòàìè êóñêà
				*(buffer + l) = input_mass[j + n * size_Chunk_ver][k + m * size_Chunk_hor];
				//cout << (int)buffer[l] << '\t';						
				l++;
				size_hor++;
			}

			size_ver++;
			//size_hor = 0;
			/*if (m == x) {
				n++;
				m = 0;
			}*/

		}

		cout << '\n';
		m++;

		if (m == x) {


			n++;
			m = 0;

		}

		//Âû÷èñëÿåì íîâûå ðàçìåðû êóñêà, ó÷èòûâàÿ íåðàâíîìåðíûå ðàçìåðû êóñêîâ
		array_chanks[i].size_colls = size_hor / size_ver;
		array_chanks[i].size_rows = size_ver;
		//Ïåðåäàžì â ôóêöèþ Binarization óêàçàòåëè íà âõîäíîé è âûõîäíîé ìàññèâû êóñêîâ è ïîðîã áèíàðèçàöèè
		Med_Filter(&array_chanks[i], &array_out_chanks[i]);
		//buffer = {0};
		size_hor = 0;
		size_ver = 0;
		l = 0;

		//Ôîðìèðîâàíèå âûõîäíîãî ìàññèâà èçîáðàæåíèÿ----------------------------------------------------------------
		//Öèêë ïî ñòîðîêàì êóñêà
        for (int j = 0; j < size_Chunk_hor; j++) {

            if (n1 * size_Chunk_hor + j >= width)
				continue;

			//Öèêë ïî ñòîëáöàì êóñêà
            for (int k = 0; k < size_Chunk_ver; k++) {
				
				//Îáðàáîòêà ñèòóàöèè, êîãäà ðàçìåð êóñêà âûõîäèò çà ãðàíèöû èçîáðàæåíèÿ
				if ((j + n1 * size_Chunk_ver > (height - 1)) || (k + m1 * size_Chunk_hor > (width - 1)))
				{
						continue;
				}
				
				//Çàïîëíÿåì ïåðâûé (âõîäíîé) áóôåð ýëåìåíòàìè êóñêà
				output_mass[j + n1 * size_Chunk_ver][k + m1 * size_Chunk_hor] = *(buffer_out + l1);
				//cout << (int)buffer_out[l] << '\t';						
				l1++;
				//size_hor++;
			}

			//size_ver++;
			//size_hor = 0;
			/*if (m == x) {
				n++;
				m = 0;
			}*/

		}


		cout << '\n';
		m1++;

		if (m1 == x) {


			n1++;
			m1 = 0;

		}
		l1 = 0;
		//--------------------------------------------------------------------------------------------------------------------


	}

	cout << '\n';

	for (int i = 0; i < height; i ++) {
		for (int j = 0; j< width; j++) {
			//input_mass[i][j] = k;
			//input_mass[i][j] = k;
			//input_mass[i][j] = k;
			//k++;;
			cout << output_mass[i][j] << '\t';
		}
		cout << '\n';
	}

	/*//Âûçîâ ôóíêöèè Áèíàðèçàöèè äëÿ êàæäîãî êóñêà--------------------
	for (int i = 0; i < count_Chunk; i ++) {
		Binarization(&array_chanks[i], &array_out_chanks[i], Step);
	}*/


	return 0;
}

