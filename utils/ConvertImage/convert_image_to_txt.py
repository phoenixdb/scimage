import sys
import Image

def main():
    img = Image.open('lena.bmp')
    i = 0
    j = 0
    y = 0
    x = 0
    text = ''
    pixels = img.load()
    size_chank_x = int(sys.argv[1])
    size_chank_y = int(sys.argv[2])
    if 256 % size_chank_x:
        count_chank_x = 256 / size_chank_x + 1
    else:
        count_chank_x = 256 / size_chank_x
    if 256 % size_chank_y:
        count_chank_y = 256 / size_chank_y + 1
    else:
        count_chank_y = 256 / size_chank_y
    while x < count_chank_x:
        while y < count_chank_y:
            text = text + '[\n'
            while i < size_chank_x:
                text = text + '['
                while j < size_chank_y:
                    if x * size_chank_x + i < 256 and y * size_chank_y + j < 256:
                        text = text + '(' + str(pixels[x * size_chank_x + i, y * size_chank_y + j]) + '),'
                    else:
                        text = text + '(),'
                    j = j + 1
                text = text[:(len(text) - 1)] + '],\n'
                i = i + 1
                j = 0
            text = text[:(len(text) - 2)] + '\n];\n'
            y = y + 1
            i = 0
        x = x + 1
        y = 0
    text = text[:(len(text) - 2)]
    f = open('../input_image.txt', 'w')
    f.write(text)
    print "Done!"
    sys.exit(0)

if __name__ == "__main__":
    main()
