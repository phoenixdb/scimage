import sys
import Image

def main():
    img = Image.new('L', (256, 256))
    pixels = img.load()
    f = open('../output_image.txt')
    text = f.read()
    i = 0
    j = 0
    k = 0
    x = 0
    y = 0
    size_chank_x = int(sys.argv[1])
    size_chank_y = int(sys.argv[2])
    if 256 % size_chank_x:
        count_chank_x = 256 / size_chank_x + 1
    else:
        count_chank_x = 256 / size_chank_x
    if 256 % size_chank_y:
        count_chank_y = 256 / size_chank_y + 1
    else:
        count_chank_y = 256 / size_chank_y
    length = len(text)
    while k < length:
        if text[k] == '{':
            position = k + 1
            while text[k] != '}':
                k = k + 1
                if text[k] == ',':
                    x = int(text[position:k]) / size_chank_x
                    position = k + 1
            y = int(text[position:k]) / size_chank_y
            i = x * size_chank_x
            j = y * size_chank_y
        if text[k] >= '0' and text[k] <= '9':
            if i < 256 and j < 256:
                if text[k + 2] >= '0' and text[k + 2] <= '9':
                    pixels[i, j] = int(text[k]) * 100 + int(text[k + 1]) * 10 + int(text[k + 2])
                    k = k + 2;
	        elif text[k + 1] >= '0' and text[k + 1] <= '9':
                    pixels[i, j] = int(text[k]) * 10 + int(text[k + 1])
                    k = k + 1;
                else:
                    pixels[i, j] = int(text[k])
                j = j + 1;
        elif text[k] == ']':
            i = i + 1
            j = y * size_chank_y
        elif text[k] == ';':
            y = y + 1
            j = y * size_chank_y
            i = x * size_chank_x
            if y == count_chank_y:
                x = x + 1;
                i = x * size_chank_x
                y = 0
                j = 0
        k = k + 1
    img.save('lena_edit.bmp')
    print "Done!"
    sys.exit(0) #success

if __name__ == "__main__":
    main()
