########################################
# BEGIN_COPYRIGHT
#
# PARADIGM4 INC.
# This file is part of the Paradigm4 Data Management and Analytics Software Library
# Copyright © 2010 - 2011 Paradigm4 Inc.
# All Rights Reserved.
#
# END_COPYRIGHT
########################################

##############################################################
# This module detects variables used in packages:
# ${DISTRO_NAME}
# ${DISTRO_VER}
# ${DISTRO_NAME_VER}
##############################################################

if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    if(EXISTS /etc/issue)
        file(READ "/etc/issue" ISSUE_STRING)
        string(REGEX MATCH "^(Red Hat|[a-zA-Z]*|Fedora) ([\\.0-9]*).*" ISSUE_STRING ${ISSUE_STRING})

        set(DISTRO_NAME ${CMAKE_MATCH_1})
        set(DISTRO_VER ${CMAKE_MATCH_2})
        if (${DISTRO_NAME} STREQUAL "Red Hat")
            set(DISTRO_NAME "RedHat")
            string(REGEX MATCH "([0-9][.][0-9])" ISSUE_STRING ${ISSUE_STRING})
            set(DISTRO_VER ${CMAKE_MATCH_1})
        endif (${DISTRO_NAME} STREQUAL "Red Hat")
        if (${DISTRO_NAME} STREQUAL "Fedora")
            string(REGEX MATCH "([12][0-9])" ISSUE_STRING ${ISSUE_STRING})
            set(DISTRO_VER ${CMAKE_MATCH_1})
        endif (${DISTRO_NAME} STREQUAL "Fedora")
	if(NOT ${DISTRO_NAME} STREQUAL "")
	    set(DISTRO_NAME_VER "${DISTRO_NAME}")
	endif(NOT ${DISTRO_NAME} STREQUAL "")
	if(NOT ${DISTRO_VER} STREQUAL "")
	    set(DISTRO_NAME_VER "${DISTRO_NAME_VER}-${DISTRO_VER}")
	endif(NOT ${DISTRO_VER} STREQUAL "")

        set(CPACK_GENERATOR TGZ)
        if (ISSUE_STRING MATCHES "Ubuntu" OR ISSUE_STRING MATCHES "Debian")
            set(CPACK_GENERATOR DEB)
        endif (ISSUE_STRING MATCHES "Ubuntu" OR ISSUE_STRING MATCHES "Debian")
        if (ISSUE_STRING MATCHES "SUSE" OR DISTRO_NAME STREQUAL "Fedora" OR DISTRO_NAME STREQUAL "RedHat")
            set(CPACK_GENERATOR RPM)
        endif (ISSUE_STRING MATCHES "SUSE" OR DISTRO_NAME STREQUAL "Fedora" OR DISTRO_NAME STREQUAL "RedHat")
    endif(EXISTS /etc/issue)
endif(${CMAKE_SYSTEM_NAME} MATCHES "Linux")

if(NOT ${DISTRO_NAME_VER} STREQUAL "")
    set(DISTRO_NAME_VER "${DISTRO_NAME}-${DISTRO_VER}")
else(NOT ${DISTRO_NAME_VER} STREQUAL "")
    set(DISTRO_NAME_VER "${CMAKE_SYSTEM_NAME}")
endif(NOT ${DISTRO_NAME_VER} STREQUAL "")
